// 배열 생성
// 새로운 배열 생성
const array1 = new Array(3)
console.log(array1) // [ , , ]
const array2 = new Array(1, 2, 3)
console.log(array2) // [ 1, 2, 3 ]
const array3 = Array.of(1, 2, 3)
console.log(array3) // [ 1, 2, 3]
// 배열 리터럴
const array4 = [1, 2, 3, 4]
console.log(array4) // [ 1, 2, 3, 4 ]
// 배열 복사 새로운 배열 객체 생성
const array5 = Array.from(array4)
console.log(array5) // [ 1, 2, 3, 4 ]

// 일반적으로 배열은 동일한 메모리 크기를 가지며, 연속적으로 이어져 있어야함
// 하지만 자바스크립트에서의 배열은 연속적으로 이어져 있지 않고
// 오브젝트와 유사함!
// 자바스크립트의 배열은 일반적인 배열읠 동작을 흉내낸 특수한 객체
// 이걸 보완하기 위해서 타입이 정해져 있는 타입 배열이 있음 (Type Collections)
const array6 = Array.from({
	0: 1,
	1: 2,
	length: 2,
})
console.log(array6) // [ 1, 2 ]
