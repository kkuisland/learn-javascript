// 배열의 함수들
// 배열 자체를 변경하는지, 새로운 배열을 반환하는지 ( Check Point )

const fruits = ['🍌', '🍎', '🍇', '🍑']

// 특정한 오브젝트가 배열인지 체크
console.log(Array.isArray(fruits)) // true
console.log(Array.isArray(Array.from({ 0: 1, 1: 2, length: 2 }))) // true
// 특정한 아이템의 위치를 찾을때
console.log(fruits.indexOf('🍎')) // 1
// 배열에 특정한 아이템이 있는지 확인
console.log(fruits.includes('🍑')) // true

// * 배열 자체를 수정 ( 업데이트 )
// 추가 - 제일 뒤
fruits.push('🍋')
console.log(fruits) // [ '🍌', '🍎', '🍇', '🍑', '🍋' ]
// 추가 - 제일 앞
fruits.unshift('🍅')
console.log(fruits) // [ '🍅', '🍌', '🍎', '🍇', '🍑', '🍋' ]
// 제거 - 제일 뒤
const lastItem = fruits.pop()
console.log(lastItem) // 🍋
console.log(fruits) // [ '🍅', '🍌', '🍎', '🍇', '🍑' ]
// 제거 - 제일 앞
const firstItem = fruits.shift()
console.log(firstItem) // 🍅
console.log(fruits) // [ '🍌', '🍎', '🍇', '🍑' ]
// 중간에 추가 또는 삭제
const middleItem = fruits.splice(1, 0, '🍅', '🍓')
console.log(middleItem) // []
console.log(fruits) // [ '🍌', '🍅', '🍓', '🍎', '🍇', '🍑' ]

// 잘라진 새로운 배열을 만듬
let newArr = fruits.slice(0, 2)
console.log(newArr) // 새로운 배열 생성 [ '🍌', '🍅' ]
console.log(fruits) // 변경되지 않는다. [ '🍌', '🍅', '🍓', '🍎', '🍇', '🍑' ]
newArr = fruits.slice(1)
console.log(newArr) // [ '🍅', '🍓', '🍎', '🍇', '🍑' ]

// 배열 합치기
const arr1 = [1, 2, 3]
const arr2 = [4, 5, 6]
const arr3 = arr1.concat(arr2)
console.log(arr3) // [ 1, 2, 3, 4, 5, 6 ]

// 배열 뒤집기
const arr4 = arr3.reverse()
console.log(arr4) // [ 6, 5, 4, 3, 2, 1 ]
