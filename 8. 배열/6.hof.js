// 고차함수 Higher Order function
const fruits = ['🍌', '🍓', '🍇', '🍓']

// 배열을 순차적으로 callback 한다
fruits.forEach((fruit, index, fruits) => {
	// 값을 출력
	console.log(fruit) // '🍌', '🍓', '🍇', '🍓'
	// 인덱스 출력
	console.log(index) // 0, 1, 2, 3
	// 배열을 출력
	console.log(fruits) // [ '🍌', '🍓', '🍇', '🍓' ] * 4
})

// 조건에 맞는(콜백함수) 아이템을 찾을때
// find: 제일 먼저 조건에 맞는 아이템을 반환
const pizza = { name: '🍕', price: 16000 }
const ramen = { name: '🍜', price: 5000 }
const sushi = { name: '🍣', price: 2000 }
const products = [pizza, ramen, sushi, ramen]

const found = products.find((product) => product.name === '🍜')
console.log(found) // { name: '🍜', price: 5000 }

// findIndex: 제일 먼저 조건에 맞는 아이템의 인덱스를 반환
const indexFound = products.findIndex((product) => product.name === '🍜')
console.log(indexFound) // 1

// some: 배열의 아이템들이 부분적으로 조건(콜백함수)에 맞는지 확인
const someFound = products.some((product) => product.name === '🍜')
console.log(someFound) // true

// every: 배열의 아이템들이 전부 조건(콜백함수)에 맞는지 확인
const everyFound = products.every((product) => product.name === '🍜')
console.log(everyFound) // false

// --- 새로운 배열 생성 ---
// filter: 조건에 맞는 모든 아이템들을 찾아 새로운 배열로 생성
const filterArray = products.filter((product) => product.name === '🍜')
console.log(filterArray) // [ { name: '🍜', price: 5000 }, { name: '🍜', price: 5000 } ]

// map: 배열의 아이템들을 각각 다른 아이템으로 매핑하여 새로운 배열 생성.
const numbers = [1, 2, 3, 4, 5]
const mapArray = numbers.map((num) => {
	// 짝수만 2곱하기
	if (num % 2 === 0) {
		return num * 2
	} else {
		return num
	}
})
console.log(mapArray) // [ 1, 4, 3, 8, 5 ]

// flatMap: 맵과 같으나 그 중 배열을 풀어서 새로운 배열 생성
const mapArr = numbers.map((num) => [1, 2])
console.log(mapArr) // [ [ 1, 2 ], [ 1, 2 ], [ 1, 2 ], [ 1, 2 ], [ 1, 2 ] ]
const flatMapArr = numbers.flatMap((num) => [1, 2])
console.log(flatMapArr) // [ 1, 2, 1, 2, 1, 2, 1, 2, 1, 2 ]
const flatMapText = ['dream', 'coding'].flatMap((text) => text.split(''))
console.log(flatMapText) // [ 'd', 'r', 'e', 'a', 'm', 'c', 'o', 'd', 'i', 'n', 'g' ]

// sort: 배열의 아이템들을 정렬
// 문자열 형태의 오름차순으로 요소를 정렬하고, ! 기존의 배열을 변경 !
const texts = ['hi', 'hello', 'abc', 1, 2, 3, 10, 4, 5, 44]
const ascending = texts.sort() // 기본 오름차순
console.log(texts) // [ 1, 10, 2, 3, 4, 44, 5, 'abc', 'hello', 'hi' ]
console.log(ascending) // [ 1, 10, 2, 3, 4, 44, 5, 'abc', 'hello', 'hi' ]
// .< 0 a 가 앞으로 정렬, 오름차순
// .> 0 b 가 앞으로 정렬, 내림차순
const descending = texts.sort((a, b) => a - b)
console.log(texts) // [ 1, 2, 3, 4, 5, 10, 44, 'abc', 'hello', 'hi' ]
console.log(descending) // [ 1, 2, 3, 4, 5, 10, 44, 'abc', 'hello', 'hi' ]

// reduce: 배열의 요소들로 하나의 결과 값을 만들어 낸다.
const reduce = [1, 2, 3, 4, 5].reduce((sum, num) => {
	sum += num
	return sum
}, 0) // sum 의 초기값 세팅 ,0
console.log(reduce) // 15
