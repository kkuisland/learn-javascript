const fruits = ['🍌', '🍎', '🍇', '🍑']

console.log(fruits[0]) // 🍌
console.log(fruits.length) // 4

// 배열을 순차적으로 꺼내기
for (let index = 0; index < fruits.length; index++) {
	const element = fruits[index]
	console.log(element) //
}

// 추가, 삭제 - 좋지 않은 방식
fruits[3] = '🍓'
delete fruits[2]
console.log(fruits) //
