// 퀴즈1: 주어진 배열 안의 딸기 아이템을 키위로 교체하는 함수를 만들기
// 단, 주어진 배열을 수정하지 않도록!
// input: ['🍌', '🍓', '🍇', '🍓']
// output: [ '🍌', '🥝', '🍇', '🥝' ]
const array1 = ['🍌', '🍓', '🍇', '🍓']

/**
 * 배열에 인자값 교체 작업
 * @param {[string]} array 과일 배열
 * @param {string} fromFruit 변경 될 과일
 * @param {string} toFruit 변경 할 과일
 * @returns [string]
 */
function replaceFruits(array, fromFruit, toFruit) {
	const newArray = array.map((item) => (item === fromFruit ? toFruit : item))
}
// 	if (item === fromFruit) {
// 		return toFruit
// 	}
// 	return item
// })
// return newArray

// 문제 의도와는 조금 다르게 개발
console.log(replaceFruits(array1, '🍓', '🥝')) // [ '🍌', '🥝', '🍇', '🥝' ]

// 퀴즈2:
// 배열과 특정한 요소를 전달받아,
// 배열안에 그 요소가 몇개나 있는지 카운트 하는 함수 만들기
// input: [ '🍌', '🥝', '🍇', '🥝' ], '🥝'
// output: 2
const array2 = ['🍌', '🥝', '🍇', '🥝']

/**
 * 과일 배열 안에서 특정 과일 갯수 카운터 해주기
 * @param {[string]} array 과일 배열
 * @param {string} fruit 찾는 과일
 * @returns number
 */
function countFruits(array, fruit) {
	const count = array.filter((item) => item === fruit)
	return count.length
}
console.log(countFruits(array2, '🥝')) // 2

// 퀴즈3: 배열1, 배열2 두개의 배열을 전달받아,
// 배열1 아이템중 배열2에 존재하는 아이템만 담고 있는 배열 반환
// input: ['🍌', '🥝', '🍇'],  ['🍌', '🍓', '🍇', '🍓']
// output: [ '🍌', '🍇' ]
const arr1 = ['🍌', '🥝', '🍇']
const arr2 = ['🍌', '🍓', '🍇', '🍓']

/**
 * 첫번째 과일 배열 과일중 두번째 과일 배열에 존재하는 과일만 반환
 * @param {[string]} arr1 첫번째 과일 배열
 * @param {[string]} arr2 두번째 과일 배열
 * @returns [string]
 */
function matchFruits(arr1, arr2) {
	const newArr = arr2.filter((item) => {
		let boolean = arr1.some((tem) => tem === item)
		console.log(boolean) //
		if (boolean) {
			return item
		}
	})
	return newArr
}

console.log(matchFruits(arr1, arr2)) // [ '🍌', '🍇' ]

// 5이상(보다 큰)의 숫자들의 평균
const numbers = [3, 16, 5, 25, 4, 34, 21]
let sum = (16 + 5 + 25 + 34 + 21) / 5
console.log(sum)

// 5 이상 배열 생성
const filterNumbers = numbers.filter((num) => num >= 5)
// 콜백함수 마다 나오는 평균 값을 전부 합하여 결과 도출 avg = value / 배열.length
const reduce = filterNumbers.reduce((avg, num, _, nums) => (avg += num / nums.length), 0)
console.log(reduce)
