// 얇은 복사 Shallow Copy - 객체는 메모리 주소 전달
// 자바스크립트에서 복사할때는 항상 얕은 복사가 이루어짐!
// Array.from, concat, slice, spread(...), Object.assign
const pizza = { name: '🍕', price: 16000 }
const ramen = { name: '🍜', price: 5000 }
const sushi = { name: '🍣', price: 2000 }
const store1 = [pizza, ramen]
const store2 = Array.from(store1)

console.log('store1', store1) // store1 [ { name: '🍕', pricprlie: 18000 }, { name: '🍜', pric: 5000 } ]
console.log('store2', store2) // store2 [ { name: '🍕', pricprlie: 18000 }, { name: '🍜', pric: 5000 } ]

store2.push(sushi)
console.log('store1', store1) // store1 [ { name: '🍕', pricprlie: 18000 }, { name: '🍜', pric: 5000 } ]
// store2 에만 스시가 추가
console.log('store2', store2) // store2 [ { name: '🍕', pricprlie: 18000 }, { name: '🍜', pric: 5000 } ]

// shallow copy 가 발생하여서 객체 값을 변경하면 모든 값이 변경 된다.
