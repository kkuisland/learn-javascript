// 옵셔널 체이닝 연산자 Optional Chaining Operator
// ES11 (ECMAScript 2020)
// ?.

// null 또는 undefined를 확인할때
let obj = { dog: '🐶', owner: { name: 'kku' } }
const ownerName = obj?.dog
console.log(ownerName)
