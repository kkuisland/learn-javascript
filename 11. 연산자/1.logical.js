// 논리연산자 Logical Operators
// && 그리고
// || 또는
// 단축평가: Short-circuit evaluation
const obj1 = { name: '🐶' }
const obj2 = { name: '🐱', owner: '밍소' }

if (obj1 && obj2) {
	console.log('둘다 true!')
}
// && And 연산자는 모두 True  이기 때문에 True 값이 아닐 때까지 검사 진행
let result = obj1 && obj2
console.log(result) // { name: '🐱', owner: '밍소' }

// || Or 연산자는 하나만 true 이어도 true 이기 때문에 true 값을 만나면 끝난다.
result = obj1 || obj2
console.log(result) // { name: '🐶' }

// default parameter는 null, undefined인 경우
// ||  falsy한 경우 설정 (0, -0, null, undefined, '')
function print(message = hi) {
	const text = message || 'Hello'
	return text
}

console.log(print(''))
console.log(print('안녕'))
