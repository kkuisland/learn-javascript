// 접근자 프로퍼티 (Accessor Property)
class Student {
	constructor(firstName, lastName) {
		this.firstName = firstName
		this.lastName = lastName
	}
	// 출력 접근 제어자
	get fullName() {
		return `${this.firstName} ${this.lastName}`
	}

	// 입력 접근 제어자
	set fullNme(value) {
		this.firstName = value.charAt(0)
		this.lastName = value.slice(1).trim()
		console.log(`${this.firstName} ${this.lastName}`)
	}
}

const 김철수 = new Student('김', '철수')
// getter 접근 제어자
console.log(김철수.fullName) // 김 철수
// setter 접근 제어자
console.log((김철수.fullNme = '김 인수')) //김 인수
console.log(김철수) // Student { firstName: '김', lastName: '인수'}
