// 카우터를 만들기
// 0 이상의 값으로 초기화 한 뒤 하나씩 숫자를 증가할 수 있는 카운터를 만들기
// Couter 클래스 만들기

class Counter {
	#count // setter 가 아니면 값을 변경 하지 못하게 설정
	constructor(startValue) {
		if (isNaN(startValue) || startValue < 0 || typeof startValue !== 'number') {
			this.#count = 0
		} else {
			this.#count = startValue
		}
	}

	// 카운터 증가
	get increment() {
		console.log(this.#count) // cout 출력
		return ++this.#count
	}

	// 카운터 리셋 0
	set reset(value) {
		this.#count = value
	}
}

const counter = new Counter(5)
console.log(counter) // Couter { count: 0 }
console.log(counter.increment) // 1
console.log(counter.increment) // 2
console.log(counter.increment) // 3
console.log((counter.reset = 0)) // 0
console.log(counter.increment) // 1
