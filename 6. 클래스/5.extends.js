// 확장 extends
class Animal {
	constructor(color) {
		this.color = color
	}

	eat() {
		console.log('먹자~!')
	}
	sleep() {
		console.log('자자zZ')
	}
}

// 단순히 상속을 받아서 Animal 기능 제공
class Tiger extends Animal {}
const tiger = new Tiger('yellow')
console.log(tiger) // Tiger { color: 'yellow' }
console.log(tiger.eat())
console.log(tiger.sleep())

// 상속을 받아서 새로운 기능 생성 및 수정
class Dog extends Animal {
	constructor(color, owner) {
		// 상속자 생성시 받아오는 parameter 를 꼭 넣어줘야한다.
		super(color) // 상속자의 생성자에 접근
		this.owner = owner
	}

	// 오버라이딩 overriding (상속자의 기능에 추가를 하거나 새로 정의 )
	eat() {
		super.eat()
		console.log('두번 먹는 개') //
	}

	play() {
		console.log('놀자~~~')
	}
}

const 누렁이 = new Dog('brown', '꾸농부')
console.log(누렁이) // Dog { color: 'brown', owner: '꾸농부' }
누렁이.eat() // Animal에 super.eat() 실행 후  새로 정의한 log 실행 ( overriding method )
누렁이.sleep() // 상속 받은 메소드
누렁이.play() // 새로 정의한 메소드
