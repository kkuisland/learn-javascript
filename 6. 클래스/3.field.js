// 접근 제어자 - 캡슐화
// private, public, protected ❌
// Javascript
// private => #, public => 기본
class Fruit {
	#name
	#emoji
	#type = '과일' // 초기화
	// 생성자 : new 키워드로 객체를 생성할때 호출되는 함수
	constructor(name, emoji) {
		this.#name = name
		this.#emoji = emoji
	}

	// instance level property method
	// 멤버 함수는 생성자 밖에서 선언 해준다. ( function 키워드 제거 )
	#display = () => {
		console.log(`${this.#name}: ${this.#emoji}`)
	}
}

// apple 은 Fruit 클래스의 인스턴스 이다
const apple = new Fruit('apple', '🍎')
console.log(apple) // Fruit {}
// console.log(apple.#name) // SyntaxError: Private field '#name' must be declared in an
