// 정직원과 파트타임직원을 나타낼 수 있는 클래스를 만들어 보자
// 직원들의 정보: 이름, 부서이름, 한달 근무 시간
// 매달 직원들의 정보를 이용해서 한달 월급을 계산할 수 있다
// 정직원은 시간당 10,000원
// 파트타임 직원은 시간당 8,000원

class Employee {
	#name
	#department
	#workingHoursPerMonth
	#payRate = 0
	constructor(name, department, workingHoursPerMonth) {
		this.#name = name
		this.#department = department
		this.#workingHoursPerMonth = workingHoursPerMonth
		// 타입스크립트는 같은 이름은 생략가능
	}

	// 근무시간 병경
	set setWorkingHoursPerMonth(hours) {
		this.#workingHoursPerMonth = hours
	}

	set setHourlyPay(payRate) {
		console.log(payRate) //
		this.#payRate = payRate
	}

	// 월 급여 계산
	get calculatePay() {
		return this.#workingHoursPerMonth * this.#payRate
	}
}

class FullTimeEmployee extends Employee {
	#payRate = 10000
	constructor(name, department, workingHoursPerMonth) {
		super(name, department, workingHoursPerMonth)
		super.setHourlyPay = this.#payRate
	}
}
const 풀풀인수 = new FullTimeEmployee('풀풀인수', '서비스', 120)
console.log(풀풀인수.calculatePay.toLocaleString('ko-kr', { maximumFractionDigits: 0 })) // 1,200,000
풀풀인수.setHourlyPay = 15000
console.log((풀풀인수.setWorkingHoursPerMonth = 130)) //
console.log(풀풀인수.calculatePay) //

const 풀인수 = new FullTimeEmployee('풀인수', '서비스', 120)
console.log(풀인수.calculatePay.toLocaleString('ko-kr', { maximumFractionDigits: 0 })) // 1,200,000
console.log((풀인수.setWorkingHoursPerMonth = 130)) //
console.log(풀인수.calculatePay) //

class PartTimeEmployee extends Employee {
	#payRate = 8000
	constructor(name, department, workingHoursPerMonth) {
		super(name, department, workingHoursPerMonth)
		super.setHourlyPay = this.#payRate
	}
}

const 파인수 = new PartTimeEmployee('파인수', '서비스', 110)
console.log(파인수.calculatePay.toLocaleString('ko-kr', { maximumFractionDigits: 0 })) // 1,200,000
