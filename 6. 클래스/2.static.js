// static 정적 프로퍼티, 메서드
class Fruit {
	// 생성자 : new 키워드로 객체를 생성할때 호출되는 함수
	constructor(name, emoji) {
		this.name = name
		this.emoji = emoji
	}

	static MAX_FRUITS = 4
	// instance level property method
	// 멤버 함수는 생성자 밖에서 선언 해준다. ( function 키워드 제거 )
	display = () => {
		console.log(`${this.name}: ${this.emoji}`)
	}

	// ! class level property method
	static makeRandomFurit() {
		// 클래스 레벨의 메서드에서는 this를 참조할 수 없음
		return new Fruit('banana', '🍌')
	}
}
// apple 은 Fruit 클래스의 인스턴스 이다
const apple = new Fruit('apple', '🍎')
console.log(apple) // Fruit { display: (), name: 'apple', emoji: 🍎 }
// orange 은 Fruit 클래스의 인스턴스 이다
const orange = new Fruit('orange', '🍊')
console.log(orange) // Fruit { display: (), name: 'orange', emoji: 🍊 }

// ! Class Level Method 사용
const banana = Fruit.makeRandomFurit()
console.log(banana) // Fruit { display: (), name: 'banana', emoji: 🍌 }
console.log(Fruit.MAX_FRUITS) // 4
// object 는 객체이고, 그 어떤 클래스의 인스턴스도 아니다.
const obj = { name: 'kku' }
