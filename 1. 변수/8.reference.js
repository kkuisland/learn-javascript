// 원시타입은 값이 복사되어 전달
let a = 1
let b = a // 1 (값 복사)
b = 2
console.log('a =>', 1)
console.log('b =>', b)

// 객체타입은 참조값(메모리주소, 레퍼런스) 가 복사되어 전달
let apple = {
	// ex 0x1234
	name: '사과',
}

let orange = apple // 0x1234  (주소 복사)
orange.name = '오랜지'
console.log('apple', apple)
console.log('orange', orange)
