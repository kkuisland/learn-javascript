// let  재할당이 가능
let a = 1
a = 2

// const 재항당이 불가능
// 1. 상수
// 2. 상수변수 또는 변수
const text = 'hello'
// text = 'hi' // Assignment to constant variable.

// 1. 상수
const MAX_FRUITS = 5

// 2. 재항당 불가능한 상수변수 또는 변수
const apple = {
	name: 'apple',
	color: 'red',
	display: '🍎',
}

// apple = {}  // Assignment to constant variable.
console.log('apple', apple)

// 객체(Object) 복합 데이터
// 객체는 주소를 참조하기 때문에 주소는 변경되지 않지만 주소 안에 값은 변경이 가능하다.
apple.name = 'orange'
apple.color = 'orange'

console.log('orange', apple)

/**
 * const 재할당만 불가능 한 선언
 * 				재할당 reassignable			 변경 Mutable
 * let		Yes											Yes
 * const	No											Yes
 * */
