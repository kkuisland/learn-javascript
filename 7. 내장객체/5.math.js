// Math Object
// static properties, method
// 오일러의 상수, 자연로그의 밑
console.log(Math.E) // 2.718281828459045
// 원주율 PI 값
console.log(Math.PI) // 3.141592653589793

// static method
// 절대값
console.log(Math.abs(-10)) // 10
// 소수점 이하 올림
console.log(Math.ceil(1.4)) // 2
// 소수점 이하 내림
console.log(Math.floor(1.4)) // 1
// 소수점 이하 반올림
console.log(Math.round(1.4)) // 1
// 정수만 반환
console.log(Math.trunc(1.5432)) // 1

// 최대, 최소값 찾기
console.log(Math.max(1, 2, 3)) // 3
console.log(Math.min(1, 2, 3)) // 1

// 거듭제곱
console.log(3 ** 2) // 9
console.log(Math.pow(3, 2)) // 9

// 제곱근
console.log(Math.sqrt(9)) // 3

// 랜던한 값을 반환  0~1 사이 값
console.log(Math.random()) // 0~1 소수점

// 1~10 정수 출력
console.log(Math.trunc(Math.random() * 10 + 1)) //
