const num1 = 123 // 원시타입
const num2 = new Number(123) // 객체
console.log(typeof num1) // number
console.log(typeof num2) // object

// 정수에서 사용가능한 최고값
console.log(Number.MAX_VALUE) // 1.7976931348623157e+308
// 정수에서 사용 가능한 최소값
console.log(Number.MIN_VALUE) // 5e-324
// 안전하게 사용할 수 있는 최대값
console.log(Number.MAX_SAFE_INTEGER) // 9007199254740991
// 안전하게 사용할 수 있는 최소값
console.log(Number.MIN_SAFE_INTEGER) // -9007199254740991
// Not a Number 숫자가 아니다
console.log(Number.NaN) // NaN
// 무한대가 아니다.
console.log(Number.NEGATIVE_INFINITY) // -Infinity
// 무한대 이다.
console.log(Number.POSITIVE_INFINITY) // Infinity

// 확인 예제
console.log(num1 === Number.NaN) // false
if (num1 === Number.NaN) {
}

console.log(Number.isNaN(num1)) // false
if (Number.isNaN(num1)) {
}

console.log(num1 < Number.MAX_VALUE) // true
if (num1 < Number.MAX_VALUE) {
}

// 지수표기법 (매우 크거나 작은 숫자를 표기할때 사용, 10의 n승으로 표기)
const num3 = 102
console.log(num3.toExponential()) // 1.02e+2
// 반올림하여 문자열로 변환
const num4 = 1234.12
// 소수점 버리고 문자열 표기
console.log(num4.toFixed()) // 1234 string
// 문자열로 변환
console.log(num4.toString()) // 1234.12 string
// 해당 국가에서 사용하는 형식으로 변경
console.log(num4.toLocaleString('ko-KR')) // 1,234.12 string

// 원하는 자릿수까지 유효하도록 반올림
console.log(num4.toPrecision(5)) // 1234.1
console.log(num4.toPrecision(4)) // 1234
// 전체 자릿수 표기가 안될때는 지수로 표기
console.log(num4.toPrecision(2)) // 1.2e+3

// 0과 1사이에서 나타낼 수 있는 가장 작은 숫자
console.log(Number.EPSILON) // 2.220446049250313e-16
if (Number.EPSILON > 0 && Number.EPSILON < 1) {
	console.log(Number.EPSILON) //
}

const num = 0.1 + 0.2 - 0.2
// 2진법 계산에서 오차 발생
console.log(num) // 0.10000000000000003

function isEqual(original, expected) {
	// return original === expected
	// 작은 값까지 감지하기 위해서
	return Math.abs(original - expected) < Number.EPSILON
}

console.log(isEqual(1, 1)) // true
console.log(isEqual(0.1, 0.1)) // true
console.log(isEqual(num, 0.1)) // true
// 작은값을 감지하지 않으면 false 출력
// console.log(isEqual(num, 0.1)) // false
