/**
 * 퀴즈!
 * 1. 문자열의 모든 캐릭터를 하나씩 출력하라
 * const text = 'Hello World!'
 * H
 * e
 * l
 * l
 * o
 * ...
 * !
 *  */
const text = 'Hello World!'
for (let index = 0; index < text.length; index++) {
	const element = text[index]
	console.log(element) // H, e, l, l, o, , W, o, r, l, d, !
}

/**
 * 2. 사용자들의 ID를 잘라내어 각각의 ID를 배열로 보관
 * const ids = 'user1, user2, user3, user4'
 * [ 'user1', 'user2', 'user3', 'user4' ]
 */
const ids = 'user1, user2, user3, user4'
console.log(ids.split(', ')) // [ 'user1', 'user2', 'user3', 'user4' ]

/**
 * 3. 1초에 한번씩 시계를 (날짜포함) 출력
 */
let now = new Date()
setInterval(() => {
	now = new Date()
	console.log(`${now.toLocaleString('ko-KR')}`) //
}, 1000)
