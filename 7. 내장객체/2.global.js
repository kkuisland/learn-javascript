// 글로벌 객체 사용
// 브라우저에서는 node와 다른 window 객체가 출력
console.log(globalThis)
console.log(this) // {}
console.log(Infinity) // Infinity
console.log(NaN) // NaN
console.log(undefined) // undefined

// 문자열을 자바 스크립트로 실행
eval('const num = 2; console.log(num)') // 2 출력
// 유한대 검사
console.log(isFinite(1)) // true
console.log(isFinite(Infinity)) // false

console.log(parseFloat('12.43')) // 12.43 nubmer
console.log(parseInt('12.43')) // 12 number
console.log(parseInt('11')) // 11 number

// URL (URI, Uniform Resource Identifier 하위 개념) 단일 리소스 식별자
// 아스키 문자로만 구성되어야 함
// 한글이나 특수문자는 이스케이프 처리 해야 한다
const URL = 'https://꾸아일랜드.com'
const encoded = encodeURI(URL)
// https://%EA%BE%B8%EC%95%84%EC%9D%BC%EB%9E%9C%EB%93%9C.com
console.log(encoded)

const decoded = decodeURI(encoded)
// https://꾸아일랜드.com
console.log(decoded)

// 전체 URL이 아니라 부분적인 것은 Component 이용
const parts = '꾸아일랜드.com'
// %EA%BE%B8%EC%95%84%EC%9D%BC%EB%9E%9C%EB%93%9C.com
console.log(encodeURIComponent(parts))
