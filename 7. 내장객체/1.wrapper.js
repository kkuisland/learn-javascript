// 래퍼 객체 (Wrapper Object)
// 원시값을 필요에 따라서 관련된 빌트인 객체로 변환한다.
const number = 123 // number 원시 타입
console.log(typeof number) // number 원시 타입

// number 원시타입을 감싸고 있는 Number  객체로 감싸짐 ( Wrapping )
console.log(typeof number.toString()) // Number Class .toString() 사용

// string 원시 타입 선언
const text = 'text'
console.log(typeof text) // string
// wrapping String 객체
console.log(typeof text.length, text.length) // number, 4
