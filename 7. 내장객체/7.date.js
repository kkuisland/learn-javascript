console.log(new Date()) // Mon Apr 11 2022 10:08:25 GMT+0900 (대한민국 표준시)
console.log(new Date('Jun 5, 2022')) // Sun Jun 05 2022 00:00:00 GMT+0900 (대한민국 표준시)
console.log(new Date('2022-04-11T10:09:00')) // Mon Apr 11 2022 10:09:00 GMT+0900 (대한민국 표준시)

// UTC기준 (협정 세계시, 1970년 1월 1일 UTC 자정과의 시간 차이를 밀리초 단위로 표기)
console.log(Date.now()) // 16496394~~~~~
// 밀리초 단위로 표현
console.log(Date.parse('2022-04-11T10:09:00')) // 1649639340000

// 인스턴스 사용
const now = new Date()
console.log(now.setFullYear(2022)) //
console.log(now.setMonth(3)) //0 부터 시작  0: 1월
console.log(now.getFullYear()) // 2023
console.log(now.getDate()) // 11
console.log(now.getDay()) // 2 {0: 일요일}
console.log(now.getTime()) // 밀리세컨드로 표현
console.log(now) // Mon Apr 11 2022 10:22:15 GMT+0900 (대한민국 표준시)

// string
console.log(now.toString()) // Mon Apr 11 2022 10:22:09 GMT+0900 (대한민국 표준시)
// Date String
console.log(now.toDateString()) // Mon Apr 11 2022
// Time String
console.log(now.toTimeString()) //10:21:58 GMT+0900 (대한민국 표준시)
// ISO 8601 형식
console.log(now.toISOString()) // 2022-04-11T01:21:54.203Z
// 지역 스타일
console.log(now.toLocaleString('en-US')) // 4/11/2022, 10:21:50 AM
console.log(now.toLocaleString('ko-KR')) // 2022. 4. 11. 오전 10:21:32
