const boolean = false // new Boolean(false)
console.log(boolean.valueOf()) // false

/**
 * Falsy
 * 0
 * -0
 * null
 * NaN
 * undefined
 * ''
 */

/**
 * Truthy
 * 1
 * -1
 * '0' 문자열 0
 * 'false' 문자열 false
 * [] 빈배열
 * {} 빈오브젝트
 */
