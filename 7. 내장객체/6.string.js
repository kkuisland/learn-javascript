// String
const textObj = new String('Hello World!')
const text = 'Hello World!'

console.log(textObj) // 한개 한개 객체로 표현
console.log(text) // 원시타입 Hello World! 표현
// 문자 갯수
console.log(text.length) // 12
// 문자열 인덱스 문자 확인
console.log(text[9]) // l
console.log(text.charAt(9)) // l

// 해당 문자가 문자열에 처음에 위치한 index 값
console.log(text.indexOf('l')) // 2
// 해당 문자가 문자열에 마지막에 위치한 index 값
console.log(text.lastIndexOf('l')) // 9
// 해당 문자가 있는지 없는지 확인 ( 대소문자 비교 )
console.log(text.includes('hell')) // false
console.log(text.includes('Hell')) // true

// 시작 문자열 확인
console.log(text.startsWith('Hell')) // true
// 끝나는 문자열 확인
console.log(text.endsWith('Hell !')) // false

// 대문자로 변경
console.log(text.toUpperCase()) // HELLO WORLD!
// 소문자로 변경
console.log(text.toLowerCase()) // hello world!

// 부분 가져오기
console.log(text.substring(0, 2)) // He
// 앞에서 부터 지정 위치부터 잘라서 가져오기
console.log(text.slice(2)) // llo world!
// 뒤에서 부터 지정 위치부터 잘라서 가져오기
console.log(text.slice(-2)) // d!

// 공백 제거
const space = '               spac e              '
console.log(space.trim()) // spac e
console.log(space.trimEnd()) // spac e
console.log(space.trimStart()) // spac e

// 원하는 문자열롤 잘라서 배열로 변환
const longText = 'Get to the point, !'
console.log(longText.split(' ')) // ['Get, 'to', 'the', 'point']
console.log(longText.split(' ', 2)) // ['Get', 'to']
console.log(longText.split(',')) // ['Get to the point', ' !']
