// Iterable 하다는건! 순회가 가능한 객체를 말한다.
// [Symbol.iterator](): IterableIterator<T>;
// 심볼정의를 가진 객체나, 특정한 함수가 IterableIterator<T>를 리턴한다는 것
// 순회 가능한 객체를 의미한다.
// 순회가 가능하면 반복문, 연산자들을 사용할 수 있다.
const array = [1, 2, 3]
console.log(array.entries()) // index, value 리턴
console.log(array.keys()) // index 리턴
console.log(array.values()) // value 리턴

for (let item of array.entries()) {
	console.log(item) // [0,1], [1,2], [2,3]
}
for (let item of array.keys()) {
	console.log(item) // 0, 1, 2
}
for (let item of array.values()) {
	console.log(item) // 1, 2, 3
}

// iterator 사용해 보기
// next() 호출 마지막 done  true , value: undefined
const iterator = array.entries()
const iterator1 = array.entries()
while (true) {
	const item = iterator.next()
	if (item.done) break
	console.log(item.value) // [ 0, 1 ], [ 1, 2 ], [ 2, 3 ]
}
console.log(iterator1.next()) // { value: [ 0, 1 ], done: false }
console.log(iterator1.next()) // { value: [ 1, 2 ], done: false }
console.log(iterator1.next()) // { value: [ 2, 3 ], done: false }
console.log(iterator1.next()) // { value: undefined, done: true }

// object is not iterable
// for of 사용불가능,  for in 으로 키 값을 받을 수 있음
const obj = { id: 123, name: 'kku' }
for (const key in obj) {
	console.log(key) // id, name
}
