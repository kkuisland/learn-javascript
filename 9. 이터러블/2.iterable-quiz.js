// [Symbol.iterator](): IterableIterator<T>;
// 0부터 10이하까지 숫자의 2배를 순회하는 이터레이터(반복자) 만들기
const 뭔지모름 = {
	[Symbol.iterator]: () => {
		const max = 10
		let num = 0
		return {
			next() {
				return { value: num++ * 2, done: num > max }
			},
		}
	},
}

const test = 뭔지모름
console.log(뭔지모름)
for (const iterator of 뭔지모름) {
	console.log(iterator)
}

// 함수화
function 이터러블함수(initialValue, maxValue, callback) {
	return {
		[Symbol.iterator]: () => {
			const max = maxValue
			let num = initialValue
			return {
				next() {
					return { value: callback(num++), done: num > max }
				},
			}
		},
	}
}

const 이터러 = 이터러블함수(0, 5, (num) => num * 5)
console.log(이터러)
for (const iterator of 이터러) {
	console.log(iterator)
}
