// 조건문 Conditional Statment
// Switch
// 정해진 범위안의 값에 대해 특정한 일을 해야 하는 경우

const fruit = '바나나'

// break 사용을 아래와 같이 범위에 값을 묶어서 표현도 가능하다.
switch (fruit) {
	case '바나나':
	case '아보카도':
	case '파인애플':
		console.log('🥝 열대과일') // 열대과일
		break

	default:
		console.log('과일')
		break
}
