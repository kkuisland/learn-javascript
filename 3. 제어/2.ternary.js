// 삼항 조건 연산자 (Ternary Operator)
// 조건식 ? 참인경우 : 거짓인경우

let fruit = 'orange'

fruit === 'apple' ? console.log('🍎') : console.log('🤦‍♂️') // 이모지 🤦‍♂️

let emoji = fruit === 'apple' ? '🍎' : fruit === 'orange' ? '🍊' : '🤦‍♂️'
console.log(emoji) // apple => 🍎,  orange => 🍊, 이외값 => 🤦‍♂️
