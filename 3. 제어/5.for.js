// 반복문 Loop Statement
// for(변수선언문; 조건식; 증감식) {}
// 실행순서:
// 1. 변수선언문
// 2. 조건식의 값이 참이면 {} 코드블럭을 수행
// 3. 증감식을 수행
// 4. 조건식이 거짓이 될때까지 2번과 3번을 반복함

// break 는 반복문 종료
// continue 아래 코드는 실행하지 않고 , 증감식을 수행후 코드 블럭을 수행
let array = [1, 2, 3, 4, 5]
for (let index = 0; index < array.length; index++) {
	const element = array[index]
	if (element === 3) {
		console.log(element) // 1
		break
	}
	// continue
	console.log(element) // 1,2,3,4,5
}
