// 퀴즈!

// num의 숫자가 짝수이면 👍, 홀수라면 👎 출력하도록
let num = 2
// if
if (num % 2 === 0) {
	console.log('👍') // 나머지 0 이면 짝수
} else {
	console.log('👎') // 나머지 0 이 아니면 홀수
}

// ternary
let result = num % 2 === 0 ? '👍' : '👎'

console.log(result) // 나머지는 0 짝수 👍
