// while(조건) {}
// 조건이 false가 될 때까지 {} 코드를 반복 실행

const isActive = false
let loopNumber = 1
while (loopNumber < 3) {
	console.log('무한루프 ➿') // 무한루프
	loopNumber++
}

// do-while
// do{} while(조건)
// 한번은 무조건 실행 후 조건이 false가 될 때까지 {} 코드를 반복 실행
loopNumber = 1
let countNumer = 0
do {
	console.log(`do ${countNumer++}`) // do-while 처음 실행
	loopNumber++
} while (loopNumber < 3)
