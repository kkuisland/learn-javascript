// 논리연산자 Logical Operator
// && 그리고
// || 또는
// ! 부정(단항연산자에서 온 것)
// !! 불리연값으로 변환 ( 단항연사자 응용버전)

let num = 8
if (num >= 0 && num <= 9) {
	// 둘 다 참이어야 실행
	console.log('👍') // 그리고 and 연산자
}

if (num >= 0 || num <= 7) {
	// 둘 중의 하나라도 참이면 실행
	console.log('👍') // 또는 or 연산자
}

num = 8
if (!!(num >= 0 && num <= 9)) {
	// 둘 다 참이어야 실행
	console.log('👍') // 그리고 and 연산자
}

// and  &&
console.log(true && true) // true
console.log(true && false) // false
console.log(false && true) // false
console.log(false && false) // false
// or  ||
console.log(true || true) // true
console.log(true || false) // true
console.log(false || true) // true
console.log(false || false) // false
// 부정 !
console.log(!true) // false
console.log(!false) // true
// boolean 값으로 변환
console.log(!!1) // true
console.log(!!!2) // false
