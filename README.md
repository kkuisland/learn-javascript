## Javascript Engine

> 브라우저 내장 자바스크립트 엔진

- 인터 프리터 방식으로 런타입시 코드를 한줄씩 번역해서 실행.
- 실행속도가 비교적 느리다. ( 많은 개선이 되어가고 있는 중 )
- (반대) Compiler 는 실행하기전 모든 코드를 컴파일링 한다.(컴파일링이 오래 걸리고 실행은 빠르다)

---

## ECMAScript

> Browser Engine

- Internet Explorer => Chakra
- Edge => V8
- Chrome => V8
- Safari => JavaScript Core
- Firefox => SpiderMonkey
- Node => V8
  > ECMAScript 문법의 규격사항, 표준사항
- ECMAScript2015 ES6 문법에 많은 변경사항 추가

---

## Javascript

Javascript(JS) is a lightweight, interpreted, or just-in-time compiled programming language with first-class functions. While it is most well-known as the scripting language for Web pages, many non-browser environments also use it, such as Node.js, Apache CouchDB and Adobbe Acrobat.
Javascript is a prototype-based, multi-paradigm, single-threaded, dynamic language, supporting object-oriented, imperative, and declarative sytles.

---

## Memory

> code : 개발자 작성 코드 \
> Data : 어플리케이션 데이터 \
> Stack : 실행 순서 \
> Heap : 객체 할당

> 입력(input) > 처리(process) > 출력(output) > 저장(storage)

---

## 변수 (Variables)

> 값을 저장하는 공간 \
> 자료를 저장할 수 있는 이름이 주어진 기억장소
