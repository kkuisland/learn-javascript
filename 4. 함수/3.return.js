function add(a, b) {
	return a + b
}

const result = add(1, 2)
console.log(result) // 3

function print(num) {
	if (num < 0) {
		return
	}
	console.log(num) //
}

// return을 명시적으로 반환하지 않으면 자동으로 undefined를 반환한다.
console.log(print(result)) // undefined
