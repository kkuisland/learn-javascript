// 매개변수의 기본값은 무조건 undefined
// 매개변수의 정보는 함수 내부에서 접근이 간으한 arguments 객체에 저장됨
// 매개변수의 기본값 Default Parameters a = 1, b = 2
function add(a = 1, b = 2, c) {
	console.log(a) //
	console.log(b) //
	console.log(c) //
	// arguments 배열로 a => arguments[0], b => arguments[1]
	console.log(arguments) //[Iterator]
	return a + b
}

console.log(add()) //

// Rest 매개변수 Rest Parameters
// 배열로 저장해준다.
function sum(a, b, ...numbers) {
	console.log(a) // 1
	console.log(b) // 2
	console.log(numbers) //[3,4,5,6,7]
}

sum(1, 2, 3, 4, 5, 6, 7)
