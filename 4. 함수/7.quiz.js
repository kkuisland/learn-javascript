// 주어진 숫자 만큼 0부터 순회하는 함수
// 순회하면서 주어진 특정한 일을 수행해야 함
// 5, 순회하는 숫자를 다 출력
// 5, 순회하는 숫자의 두배값을 다 출력
// function iterate(max, action)

const numbericOuput = (number) => {
	console.log(number) // number
}
const numbericOuputX2 = (number) => {
	console.log(number * 2) // number
}

function iterate(number = 0, action) {
	console.log(number) // 5,5
	console.log(action) // numbericOuput, numbericOuputX2
	for (let index = 0; index < number; index++) {
		action(index)
	}
}

// console.log(iterate(5, numbericOuput)) // undefined
// iterate(5, (number) => console.log(number))
// console.log(iterate(5, numbericOuputX2)) // undefined
// iterate(5, (number) => console.log(number * 2)) // undefined

setTimeout(() => {
	console.log('3초 뒤에 실행')
}, 3000)
