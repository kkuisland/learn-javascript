// 함수 선언문 function name() {}
// 함수 표현식 const name = function() {}
// 익명함수
const add = function (a, b) {
	// this 함수 내부 객체 사용
	// console.log(this) // ... 많은 내용
	return a + b
}
console.log(add(1, 2)) // 3

// 화살표 함수 const name = > () => {}
const sum = (a, b) => {
	// this 빈 객체
	console.log(this) // {}
	return a + b
}

console.log(sum(2, 3)) // 5

// IIFE (Immediately-Invoked Function Expressions)
;(function run() {
	console.log('🛩️') // 즉시실행 🛩️
})()
