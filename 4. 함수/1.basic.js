function add(num1, num2) {
	const result = num1 + num2
	console.log(result) // num1 + num2 = result
	return result
}

console.log(add(1, 2)) // parameter1 + parameter2 = result

// 함수의 사용이유 ( 반복적인 작업, 효율적인 수정, 의미있는 가독성 명칭사용 ...)
function makeFullName(firstName, lastName) {
	return `${firstName} ${lastName}`
}

let firstName = '김'
let lastName = '인수'
console.log(makeFullName(firstName, lastName)) // 김 인수
firstName = '구'
lastName = '인수'
console.log(makeFullName(firstName, lastName)) // 구 인수
