// 함수내부에서 외부로부터 주어진 인자값을 변경하는 것은 💩
// 상태변경이 필요한 경우에는, 새로운 (오브젝트, 값) 만들어서 반환해야 한다.
// 원시값 - 값에 의한 복사
// 객체값 - 참조에 의한 복사 (메모리주소)
function display(num) {
	num = 5 // let num = value 4 값을 복사 하는 상황
	console.log(num) // 5
}

const value = 4
display(4)
console.log(value) // 4

function displayName(person) {
	person.name = '구인수'
	console.log(person) // { name: '구인수'}
}

const 고길동 = { name: '고길동' }
displayName(고길동)
console.log(고길동) // {name: '구인수'}

function changeName(person) {
	const newOne = { ...person }
	console.log(newOne === person) // 주소값이 다른걸 확인
	return { ...person, name: '구인수' }
}
const 꾸길동 = { name: '꾸길동' }
changeName(꾸길동)
console.log(꾸길동) // {name: '꾸길동}
