// 증감 & 감소 연사자 (Increment & Decrement Operators)
let a = 0
console.log(a)

a++
console.log(a) // 1=> a = a + 1
a--
console.log(a) // 0 => a = a - 1

// !주의

// a++ 필요한 연산을 하고, 그 뒤에 값을 증가시킴
// ++a 값을 먼저 증가하고, 필요한 연산을 함

a = 0

let b = a++ // 할당이 먼저 일어난다  b = 0 (a) , 이후 증가가 일어난다.
console.log(b) // 0
console.log(a) // 1

a = 0

b = ++a // 증가가 먼저 일어나고 할당이 일어난다. b = 1 (a)
console.log(b) // 1
console.log(a) // 1
