// 산술 연산자 (arithmetic operators)
// + 더하기
// - 빼기
// * 곱
// / 나누기
// % 나머지 값
// ** 지수 (거듭제곱)

console.log('+ 더하기 => 5 + 2 = ', 5 + 2)
console.log('- 빼기 => 5 - 2 = ', 5 - 2)
console.log('* 곱 => 5 * 2 = ', 5 * 2)
console.log('/ 나누기 => 5 / 2 = ', 5 / 2)
console.log('% 나머지 값 => 5 % 2 = ', 5 % 2)
console.log('** 지수 => 5 ** 2 = ', 5 ** 2)

// + 연산자 주의점!
let text = '두개의' + '문자를'
console.log(text)

text = '1' + 1 // 숫자와 문자열을 더하면 문자열로 변환됨
console.log(text)
