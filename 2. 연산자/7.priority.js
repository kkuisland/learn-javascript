// 우선순위 연산자 (Priority Operators)

let a = 2
let b = 3
let result = a + b * 4
console.log(result) // b * 4 + a = 14
result = a++ + b * 4
console.log(result) // b * 4 + a(2) = 14  이후 a 3 으로 변경
result = ++a + b * 4
console.log(result) // b * 4 + a(3) = 16

// https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Operators/Operator_Precedence
