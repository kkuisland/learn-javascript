// 동등 비교 관계 연산자 ( Equality Operators)
// == 같이 같음
// != 값이 다름
// === 값과 타입이 둘다 같음
// !== 값과 타입이 둘다 다름

console.log(2 == 2) // true
console.log(2 != 2) // false
console.log(2 != 3) // true
console.log(2 == 3) // false
console.log(2 == '2') // true
console.log(2 === '2') // false
console.log(true == 1) // true
console.log(true === 1) // false
console.log(false == 0) // true
console.log(false === 0) // false

const object1 = {
	name: 'js',
	number: 1,
}

const object2 = {
	name: 'js',
	number: '1',
}

// 서로 다른 메모리 주소를 가지고 있기 때문에 다르게 판단
console.log(object1 == object2) // false

// 값은 타입과 값까지 동일하기 때문에 True 판단
console.log(object1.name == object2.name) // true
console.log(object1.name === object2.name) // true
console.log(object1.number == object2.number) // true
console.log(object1.number === object2.number) // false

const object3 = object2
// 동일한 주소값을 가지고 있기 때문에 같다 판단
console.log(object3 == object2) // true
console.log(object3 === object2) // true
console.log(object2.name == object3.name) // true
console.log(object2.name === object3.name) // true
