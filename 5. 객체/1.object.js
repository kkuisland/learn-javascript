// 객체 Object literal {key: value}
// new Object()
// Object.create()
// key - 문자, 숫자, 문자열, 심볼
// value - 원시값, 객체 ( 함수 )

const apple = {
	name: 'apple',
	hello: '🖐',
	0: 1,
	['bye']: '👋🏽',
}

// 속성, 데이터에 접근
console.log(apple.name) // 마침표 표기법 dot notation
console.log(apple['hello']) // 대괄호 표기법 bracket notation
console.log(apple[0]) // 1
console.log(apple['bye']) // 👋🏽

// 속성 추가
apple.price = 2000
console.log(apple) //

// 속성 삭제
delete apple.price
delete apple.price
delete apple[0]
console.log(apple) //{name: 'apple', hello: 🖐, bye: 👋}

// ES2015 / ES5
let name = 'apple5'
let bye = '👋'
let hello = 'hello'

const apple5 = { name, bye, [hello]: '🖐', ['by' + 'e']: '👋🏽1' }
console.log(apple5) //

/**
 *
 * ECMAScript 5 엄격 모드 코드에서는 중복된 프로퍼티명을 SyntaxError로 간주합니다.
 * 런타임 시 복제를 가능하게 한 계산된 프로퍼티 명의 도입으로
 * ECMScript 2015는 이 제한을 제거했습니다.
 */
function haveES2015DuplicatePropertySemantics() {
	'use strict'
	try {
		;({ prop: 1, prop: 2 })
		// 에러가 발생하지 않음, 중복된프로퍼티명이 엄격 모드에서 허용됨
		return true
	} catch (error) {
		//에러가 발생함, 엄격 모드에서 중복이 금지
		return false
	}
}

console.log(haveES2015DuplicatePropertySemantics()) // true

/**
 * ES5 getter/setter
 * 다음의 ES5와 유사한 표기법과 동일합니다
 * (하지만 ECMAScript 5에는 generator가 없음을 유의하세요).
 */
let o = {
	apple: {},
	property1: function (parameters) {
		console.log('test') //
	},
	get property() {
		console.log('getter') // getter
		return this.apple.a
	},
	set property(value) {
		this.apple.a = value
		console.log(value) //
	},
	*generator() {},
}

// setter
console.log((o.property = 1)) // {property: [Getter/Setter]}
// getter
console.log(o.property) // {property: [Getter/Setter]}

o.property1(1)

// yield  ( 제너럴 )
const obj2 = {
	// g: function*() {}
	*g() {
		let index = 0
		while (true) {
			yield index++
		}
	},
}
console.log(obj2.g().next()) // 0
// index 0 실행
console.log(obj2.g().next()) // 0

// 참조 후 next  index = 0 실행 되지 않는다.
const it = obj2.g()
console.log(it.next()) // {value: 0, done: false}
console.log(it.next()) // {value: 1, done: false}

/**
 * * 계산된 프로퍼티명 Computed property names
 * ECMAScript 2015부터 객체 초기자 구문은 계산된 프로퍼티 명도 지원합니다.
 * 대괄호 [] 안에서 표현식을 허용하며, 표현식은 프로퍼티명으로 계산되어 사용됩니다.
 * 이는 이미 프로퍼티를 읽고 설정하는 데 사용하는 프로퍼티 접근자 구문의 대괄호 표기법을 연상시킵니다.
 * */
let i = 0
let a = {
	['foo' + ++i]: i,
	['foo' + ++i]: i,
	['foo' + ++i]: i,
	['foo' + ++i]: i,
}
console.log(a.foo1) // 1
console.log(a.foo2) // 2
console.log(a.foo3) // 3
console.log(a.foo4) // 4
console.log(a.foo5) // undefined

// 배열을 이용 ( 문자열로 인식 )
const items = ['A', 'B', 1]
const obj = {
	[items]: 'Hello',
}

console.log(obj) // { 'A,B,C': 'Hello }
console.log(obj['A,B,1']) // Hello

// 좀 더 복잡한 computed property
let param = 'size'
let config = {
	[param]: 12,
	['modile' + param.charAt(0).toUpperCase() + param.slice(1)]: 4,
}

console.log(config) // { size: 12, modileSize: 4 }

/**
 * * 전개 속성 Spread properties
 * ECMAScript의 나머지/전개 프로퍼티 제안(stage 4)은 객체 리터럴에 전개 프로퍼티를 추가합니다.
 * 이는 제공된 객체로부터 새로운 객체로 자신만의 열거형 프로퍼티를 복사합니다.
 */
let spreadObj1 = {
	foo: 'bar',
	x: 42,
	get sset() {
		return false
	},
	set sset(value) {
		console.log(value) //
		this.x = value
	},
}
let spreadObj2 = { foo: 'baz', y: 13 }

let clonedObj = { ...spreadObj1 }

console.log(clonedObj) // { foo: 'bar', x: 42 }

let mergedObj = { ...spreadObj1, ...spreadObj2 }
console.log(mergedObj) // {food: 'baz', x:42, y: 13}
mergedObj.sset = 32
console.log(mergedObj) // {food: 'baz', x:42, y: 13}

// !경고: Object.assign()은 setters를 트리거하는 반면 전개 연산자는 그렇지 않음을 유의하세요!
let mergedObj2 = Object.assign(spreadObj1, spreadObj2)
console.log(mergedObj2) // { foo: 'baz', x: 42, sset: [Getter/Setter], y: 13}
mergedObj2.sset = 32
console.log(mergedObj2) // { foo: 'baz', x: 42, sset: [Getter/Setter], y: 13}

// 프로토 타입 변형  Prototype mutation
let obj11 = {}
console.log(Object.getPrototypeOf(obj11)) // {}
console.log(Object.prototype) // {}
console.log(Object.getPrototypeOf(obj11) === Object.prototype) // true

let obj22 = { __proto__: null }
console.log(Object.getPrototypeOf(obj22)) // null
console.log(Object.getPrototypeOf(obj22) === null) // true

let protoObj = {}
let obj33 = { __proto__: protoObj }
console.log(Object.getPrototypeOf(obj33)) // {}
console.log(protoObj) // {}
console.log(Object.getPrototypeOf(obj33) === protoObj) // true

let obj44 = { __proto__: 'not an object or null' }
console.log(Object.getPrototypeOf(obj44)) // {}
console.log(Object.getPrototypeOf(obj44) === Object.prototype) // true
console.log(obj44.hasOwnProperty('__proto__')) // false
console.log(!obj44.hasOwnProperty('__proto__')) // true
