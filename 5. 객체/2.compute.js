const obj = {
	name: '꾸',
	age: 37,
}

// 코딩하는 시점에, 정적으로 접근이 확정
obj.name // 꾸
obj.age // 37

// 동적으로 속성에 접근하고 싶을때 대괄호 표기법 사용
function getValue(obj, key) {
	console.log(obj, key) // { name: '꾸', age: 37 }, 'name'
	return obj[key]
}

console.log(getValue(obj, 'name')) // 꾸
