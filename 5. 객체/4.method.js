// 데이터 (property)
// 순수 데이터 객체
const orange = {
	name: 'orange',
	price: 2000,
}
// 함수 (method)
// 상태와 행동  객체
// 객체 안에 상태를 나타낼 수 있는 메소드 사용
const apple = {
	name: 'apple',
	display: function () {
		console.log(`${this.name}: 🍎`)
	},
}

apple.display() // apple: 🍎
