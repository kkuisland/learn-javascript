const apple = {
	name: 'apple',
	display: function () {
		console.log('🍎')
	},
}

const orange = {
	name: 'orange',
	display: function () {
		console.log('🍊')
	},
}
console.log(apple) // { name: apple, display: apple: 🍎 }
console.log(orange) // { name: orange, display: orange: 🍊 }

// 생성자 함수
function Fruit(name, emoji) {
	this.name = name
	this.emoji = emoji
	this.display = () => {
		console.log(`${this.name}: ${this.emoji}`) // { this.name: this.emoji }
	}

	// return this 자동 생략
}
const newApple = new Fruit('apple', '🍎')
console.log(newApple) // Fruit { name: 'apple', emoji: 🍎, display: apple: 🍎 }
const newOrange = new Fruit('orange', '🍊')
console.log(newOrange) // Fruit { name: 'orange', emoji: 🍊, display: orange: 🍊 }
