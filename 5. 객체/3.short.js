const x = 0
const y = 0

const coordinate = { x, y }
console.log(coordinate) // { x; 0, y: 0 }

function makeObj(name, age) {
	return {
		// name: name,
		name, // short 방식
		// age: age,
		age, // short 방식
	}
}

console.log(makeObj('kku', 37)) // { name: 'kku', age: 37 }
